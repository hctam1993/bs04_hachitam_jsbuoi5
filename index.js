// ================= Bài 1
function diemKhuVuc(khuVuc) {
  var diem = 0;
  if (khuVuc == "A") {
    diem = 2;
  } else if (khuVuc == "B") {
    diem = 1;
  } else {
    diem = 0.5;
  }
  return diem;
}
function diemDoiTuong(doiTuong) {
  var diem = 0;
  if (doiTuong == "1") {
    diem = 2.5;
  } else if (doiTuong == "2") {
    diem = 1.5;
  } else {
    diem = 1;
  }
  return diem;
}
function xetDiem() {
  var diemChuan = document.getElementById("txt_diem_chuan").value * 1;
  var diemMon1 = document.getElementById("txt_diem_1").value * 1;
  var diemMon2 = document.getElementById("txt_diem_2").value * 1;
  var diemMon3 = document.getElementById("txt_diem_3").value * 1;
  var khuVuc = document.getElementById("txt_khu_vuc").value;
  //   console.log("khuVuc: ", khuVuc);
  var doiTuong = document.getElementById("txt_doi_tuong").value;
  //   console.log("doiTuong: ", doiTuong);
  var diem =
    diemMon1 +
    diemMon2 +
    diemMon3 +
    diemKhuVuc(khuVuc) +
    diemDoiTuong(doiTuong);

  diem >= diemChuan
    ? (document.getElementById(
        "result1"
      ).innerText = ` Bạn đã đậu! Bạn được ${diem} điểm.`)
    : (document.getElementById(
        "result1"
      ).innerText = ` Bạn đã rớt! Bạn được ${diem} điểm.`);
}
// ================= Bài 2
function tinhTien() {
  var soDien = document.getElementById("txt_so_dien").value * 1;

  var tongTien = 0;
  if (soDien <= 50) {
    tongTien = 500 * soDien;
  } else if (soDien <= 100) {
    tongTien = 500 * 50 + (soDien - 50) * 650;
  } else if (soDien <= 200) {
    tongTien = 500 * 50 + 50 * 650 + (soDien - 100) * 850;
  } else if (soDien <= 350) {
    tongTien = 500 * 50 + 50 * 650 + 100 * 850 + (soDien - 200) * 1100;
  } else {
    tongTien =
      500 * 50 + 50 * 650 + 100 * 850 + 150 * 1100 + (soDien - 350) * 1300;
  }
  document.getElementById("result2").innerText = tongTien;
}
